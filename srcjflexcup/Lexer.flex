package it.unisa.compilatori;
import java_cup.runtime.*;
import java.lang.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;


%%

%class Lexer
%unicode
%cupsym sym
%cup
%line
%column

%{
    HashMap<String, SymbolCustom> symbolTable = new HashMap<>();
    Set<Character> SEPARATORS;

    StringBuffer string = new StringBuffer();
    private SymbolCustom symbol(int type, String name) {
        return new SymbolCustom(type, name, yyline, yycolumn, name);
    }
    public SymbolCustom symbol(int type, String name, Object value) {
        return new SymbolCustom(type, name, yyline, yycolumn, value);
    }


    private void populateKeyWord(){
        symbolTable.put("if",       symbol(sym.IF,      sym.terminalNames[sym.IF]));
        symbolTable.put("then",     symbol(sym.THEN,    sym.terminalNames[sym.THEN]));
        symbolTable.put("else",     symbol(sym.ELSE,    sym.terminalNames[sym.ELSE]));
        symbolTable.put("while",    symbol(sym.WHILE,   sym.terminalNames[sym.WHILE]));
        symbolTable.put("int",      symbol(sym.INT,     sym.terminalNames[sym.INT]));
        symbolTable.put("float",    symbol(sym.FLOAT,   sym.terminalNames[sym.FLOAT]));
        symbolTable.put("function", symbol(sym.FUNCTION,sym.terminalNames[sym.FUNCTION]));
        symbolTable.put("main",     symbol(sym.MAIN,    sym.terminalNames[sym.MAIN]));
        symbolTable.put("end",      symbol(sym.END,     sym.terminalNames[sym.END]));
        symbolTable.put("do",       symbol(sym.DO,      sym.terminalNames[sym.DO]));
        symbolTable.put("local",    symbol(sym.LOCAL,   sym.terminalNames[sym.LOCAL]));
        symbolTable.put("return",   symbol(sym.RETURN,  sym.terminalNames[sym.RETURN]));
        symbolTable.put("for",      symbol(sym.FOR,     sym.terminalNames[sym.FOR]));
        symbolTable.put("nil",      symbol(sym.NIL,     sym.terminalNames[sym.NIL])); //UP
        symbolTable.put("bool",     symbol(sym.BOOL,    sym.terminalNames[sym.BOOL]));
        symbolTable.put("string",   symbol(sym.STRING,  sym.terminalNames[sym.STRING]));
        symbolTable.put("nop",      symbol(sym.NOP,     sym.terminalNames[sym.NOP]));
        symbolTable.put("true",     symbol(sym.BOOL_CONST,    sym.terminalNames[sym.BOOL_CONST],        sym.terminalNames[sym.TRUE])); //UP
        symbolTable.put("false",    symbol(sym.BOOL_CONST,    sym.terminalNames[sym.BOOL_CONST],        sym.terminalNames[sym.FALSE])); // UP
        symbolTable.put("not",      symbol(sym.NOT,     sym.terminalNames[sym.NOT]));
        symbolTable.put("global",   symbol(sym.GLOBAL,  sym.terminalNames[sym.GLOBAL]));
        symbolTable.put("and",      symbol(sym.AND,     sym.terminalNames[sym.AND]));
        symbolTable.put("or",       symbol(sym.OR,      sym.terminalNames[sym.OR]));

    }


    private void installIdentifier(String lessema){
        if(symbolTable.containsKey(lessema))
            return;
        symbolTable.put(lessema, symbol(sym.ID, sym.terminalNames[sym.ID], lessema));

    }

    private void installIntegerNumber(String lessema){
        if (symbolTable.containsKey(lessema))
            return;
        symbolTable.put(lessema, symbol(sym.INT_CONST, sym.terminalNames[sym.INT_CONST], lessema));
    }

    private void installFloatNumber(String lessema){
        if (symbolTable.containsKey(lessema))
            return;
        symbolTable.put(lessema, symbol(sym.FLOAT_CONST, sym.terminalNames[sym.FLOAT_CONST], lessema));
    }

    private void installString(String lessema){
        if(symbolTable.containsKey(lessema))
            return;
        symbolTable.put(lessema,symbol(sym.STRING_CONST, sym.terminalNames[sym.STRING_CONST], lessema));
    }
%}

%init{

    populateKeyWord();

%init}


LineTerminator = \r|\n|\r\n
//InputCharacter = [^\r\n]
WhiteSpace = {LineTerminator} | [ \t\f\n]

Identifier = [:jletter:] [:jletterdigit:]*
Expo = ([eE][-+]? [0-9]+)
DecIntegerConstant = 0 | [+-]?[1-9][0-9]*
DecFloatConstant = [-+]?(0 | [1-9][0-9]*)\.( [0] | [0-9]*[1-9]){Expo}?
%state STRING_CONST

%%


/* keywords */
<YYINITIAL> {
        "if"            { return symbol(sym.IF,        sym.terminalNames[sym.IF]); }
        "then"          { return symbol(sym.THEN,      sym.terminalNames[sym.THEN]); }
        "else"          { return symbol(sym.ELSE,      sym.terminalNames[sym.ELSE]); }
        "while"         { return symbol(sym.WHILE,     sym.terminalNames[sym.WHILE]); }
        "int"           { return symbol(sym.INT,       sym.terminalNames[sym.INT]); }
        "float"         { return symbol(sym.FLOAT,     sym.terminalNames[sym.FLOAT]); }
        "function"      { return symbol(sym.FUNCTION,  sym.terminalNames[sym.FUNCTION]); }
        "main"          { return symbol(sym.MAIN,      sym.terminalNames[sym.MAIN]); }
        "end"           { return symbol(sym.END,       sym.terminalNames[sym.END]); }
        "do"            { return symbol(sym.DO,        sym.terminalNames[sym.DO]); }
        "local"         { return symbol(sym.LOCAL,     sym.terminalNames[sym.LOCAL]); }
        "return"        { return symbol(sym.RETURN,    sym.terminalNames[sym.RETURN]); }
        "for"           { return symbol(sym.FOR,       sym.terminalNames[sym.FOR]); }
        "nil"           { return symbol(sym.NIL,       sym.terminalNames[sym.NIL]); }
        "bool"          { return symbol(sym.BOOL,      sym.terminalNames[sym.BOOL]); }
        "string"        { return symbol(sym.STRING,    sym.terminalNames[sym.STRING]); }
        "nop"           { return symbol(sym.NOP,       sym.terminalNames[sym.NOP]); }
        "true"          { return symbol(sym.TRUE,      sym.terminalNames[sym.TRUE]); }
        "false"         { return symbol(sym.FALSE,     sym.terminalNames[sym.FALSE]); }
        "global"        { return symbol(sym.GLOBAL,    sym.terminalNames[sym.GLOBAL]); }
}


<YYINITIAL> {

    /* operators */
        "="             {return symbol(sym.ASSIGN,     sym.terminalNames[sym.ASSIGN]); }
        "and"           {return symbol(sym.AND,        sym.terminalNames[sym.AND]); }
        "or"            {return symbol(sym.OR,         sym.terminalNames[sym.OR]); }
        ">"             {return symbol(sym.GT,         sym.terminalNames[sym.GT],      ">"); }
        ">="            {return symbol(sym.GE,         sym.terminalNames[sym.GE],      ">="); }
        "<"             {return symbol(sym.LT,         sym.terminalNames[sym.LT],      "<"); }
        "<="            {return symbol(sym.LE,         sym.terminalNames[sym.LE],      "<="); }
        "=="            {return symbol(sym.EQ,         sym.terminalNames[sym.EQ],      "=="); }
        "not"           {return symbol(sym.NOT,        sym.terminalNames[sym.NOT]); }
        "#"             {return symbol(sym.SHARP,      sym.terminalNames[sym.SHARP]); }
        "+"             {return symbol(sym.PLUS,       sym.terminalNames[sym.PLUS]); }
        "-"             {return symbol(sym.MINUS,      sym.terminalNames[sym.MINUS]); }
        "*"             {return symbol(sym.TIMES,      sym.terminalNames[sym.TIMES]); }
        "/"             {return symbol(sym.DIV,        sym.terminalNames[sym.DIV]); }
        "<=="           {return symbol(sym.READ,       sym.terminalNames[sym.READ]); }
        "==>"           {return symbol(sym.WRITE,      sym.terminalNames[sym.WRITE]); }
        "!="            {return symbol(sym.NE,         sym.terminalNames[sym.NE],      "!="); }


    /* identifiers */
    {Identifier} {
                        installIdentifier(yytext());
                        return symbol(sym.ID, sym.terminalNames[sym.ID], yytext());
    }

   /* CONSTANTS */

    {DecFloatConstant}      {
                        installFloatNumber(yytext());
                        return symbol(sym.FLOAT_CONST, sym.terminalNames[sym.FLOAT_CONST], yytext());
    }

    {DecIntegerConstant}    {
                        installIntegerNumber(yytext());
                        return symbol(sym.INT_CONST, sym.terminalNames[sym.INT_CONST], yytext());
    }

    \"                      {
                        string.setLength(0);
                        yybegin(STRING_CONST);
    }



    /* whitespace */
    {WhiteSpace}    { /* ignore */ }

    /* Separators */
    ";"                 {return symbol(sym.SEMI,       sym.terminalNames[sym.SEMI],    ";"); }
    ","                 {return symbol(sym.COMMA,      sym.terminalNames[sym.COMMA],   ","); }
    "("                 {return symbol(sym.LPAR,       sym.terminalNames[sym.LPAR],    "("); }
    ")"                 {return symbol(sym.RPAR,       sym.terminalNames[sym.RPAR],    ")"); }
    ":"                 {return symbol(sym.COLON,      sym.terminalNames[sym.COLON],   ":"); }
    "{"                 {return symbol(sym.BLPAR,      sym.terminalNames[sym.BLPAR],   "\u007B"); }
    "}"                 {return symbol(sym.BRPAR,      sym.terminalNames[sym.BRPAR],   "\u007D"); }
    "["                 {return symbol(sym.SLPAR,      sym.terminalNames[sym.SLPAR],   "\u005B"); }
    "]"                 {return symbol(sym.SRPAR,      sym.terminalNames[sym.SRPAR],   "\u005D"); }


}


<STRING_CONST> {
    \"              {
                        yybegin(YYINITIAL);
                        installString(string.toString());
                        return symbol(sym.STRING_CONST, sym.terminalNames[sym.STRING_CONST], string.toString());
                    }
    [^\n\r\"\\]+    { string.append( yytext() ); }
    \\t             { string.append('\t'); }
    \\n             { string.append("\\n"); }
    \\r             { string.append('\r'); }
    \\\"            { string.append('\"'); }
    \\              { string.append('\\'); }
}

/* error fallback */
[^] { throw new Error("Illegal character <"+yytext()+">,line: " + yyline + " column: " + yycolumn); }
//<<EOF>> {return new SymbolCustom("EOF", sym.EOF);}
