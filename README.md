# Progetto MyPallene
Paolo Vigorito (0522500700)

Paolo Marrone (0522500701)

## Modifiche strutturali 
Abbiamo riscontrato due problemi nella grammatica:

### 1
Problema: Nella grammatica originale è possibile definire funzioni senza parametri ma non è possibile fare chiamate a funzione con meno di 1 argomento.

Soluzione: Aggiunte le seguenti produzioni alla grammatica:

- stat ==> ID LPAR RPAR
- expr ==> ID LPAR RPAR

e gestite di conseguenza nelle fasi successive del compilatore.

### 2
Problema: Nella tabella op2 dei tipi la divisione tra interi restituisce un intero invece che un float

Soluzione: Modificata op2 in modo che una divisione tra interi restituisca float.

NOTA: Abbiamo quindi inteso la divisione come divisione reale e non come la divisione tra interi di C, quale tronca le cifre decimali.


## Esecuzione
Le dipendenze sono
- java11
- clang2-7

Nella cartella del progetto è sufficiente eseguire lo script *mypallenecompiler.sh file di input*. Ad esempio:

./mypallenecompiler inputs/4_menu_operazioni