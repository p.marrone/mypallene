package it.unisa.compilatori;


public class Utils {

    // PROGRAM OP ELEMENT's POSITION
    public static final int PROGRAMOP_GLOBAL_BLOCK_POS=0,
                            PROGRAMOP_FUNC_LIST_POS = 1,



    //FUNCTION ELEMENT'S POSITION
                            FUN_DECLS_POS = 1,
                            FUNCTION_DEF_ID_POS=0,
                            FUNCALLOP_ID_POS=0,
                            FUNCALLOP_EXPRS_LIST=1,
                            FUNCTION_DEF_PARDECLS_LIST_POS = 1,
                            FUNCTION_DEF_TYPE_POS_NO_PARAMS=1,
                            FUNCTION_DEF_TYPE_POS_WITH_PARAMS=2,
    // GLOBAL BLOCK ELEMENT'S POSITION
                            GLOBAL_BLOCK_ASSIGN_POS=2,
                            GLOBAL_BLOCK_VAR_DECLS_LIST_POS=0,


    // BODY OP ELEMENT'S POSITION
                            BODYOP_POS_PARAMS=3,
                            BODYOP_POS_NO_PARAMS=2,


    // ID ASSIGNMENT VAR ELEMENT's POSITION
                            ID_ASSIGNMENT_VAR_POS=0,
                            ID_ASSIGNMENT_EXPR_POS=1,


    // EXPR  ELEMENT's POSITION
                            EXPR_FIRST_CHILD_POS=0,
                            EXPR_SECOND_CHILD_POS=1,


    // WHILE ELEMENT's POSITION

                            WHILE_COND_POS = 0,
                            WHILE_BODY_POS = 1,


    // IF ELEMENT's POSITION
                             IF_COND_POS = 0,
                             IF_BODY_POS = 1,
                             IF_ELSE_BODY_POS = 2,


    // FOR ELEMENT's POSITION
                            FOR_ID_POS = 0,
                            FOR_COND_POS = 1,
                            FOR_BODY_POS = 2,


    // RETURN ELEMENT's POSITION
                            RETURN_BODY_POS = 0,


    // VAR DECL ELEMENT's POSITION
                            VAR_DECL_ID_POS=0,
                            VAR_DECL_EXPR_POS = 2,
                            VAR_DECL_TYPE_POS=1,
                            VAR_DECL_SIZE_WITH_ASSIGN=3,


    // LOCAL ELEMENT's POSITION
                            LOCAL_BLOCK_VARDECLS_POS = 0,
                            LOCAL_BLOCK_BODY_POS = 1,

    // PAR DECL ID POS
                            PAR_DECL_ID_POS=0,
                            PAR_DECL_TYPE_POS=1,


    // READ OP ELEMENT's POSITION
                            READ_OP_IDs_LIST_POS = 0;

}
