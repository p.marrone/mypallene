package it.unisa.compilatori;

import it.unisa.compilatori.exceptions.CodeGenerationException;
import it.unisa.compilatori.visitor.VisitableNode;

import java.util.HashMap;

public class CodeGenerator {

    private VisitableNode root;
    private StringBuilder content;
    HashMap<String, SymbolCustom> symbolTable;


    /**
     * Constructor
     * @param root root node of the syntax tree
     * @param symbolTable the lexer's symbol table
     */
    public CodeGenerator(VisitableNode root, HashMap<String, SymbolCustom> symbolTable) {
        this.root = root;
        content = new StringBuilder();
        this.symbolTable = symbolTable;
    }


    /**
     * It generates a C program starting from the syntax tree that has been passed to the constructor
     * The C code will be in the content variable
     * @throws CodeGenerationException in case of a not expected/implemented code transformation
     */
    public void codeC() throws CodeGenerationException {

        // Headers
        content.append(getHeaders());

        int funcListPos = root.sons().get(Utils.PROGRAMOP_GLOBAL_BLOCK_POS).data().getSymClass() == Constants.GLOBAL_BLOCK_SYM ? Utils.PROGRAMOP_FUNC_LIST_POS : 0;

        // Global block -- Declaration (Init is later done by a function in order to make the usage of function calls in the assignment)
        if (root.sons().get(Utils.PROGRAMOP_GLOBAL_BLOCK_POS).data().getSymClass() == Constants.GLOBAL_BLOCK_SYM) {
            VisitableNode vardecls = root.sons().get(Utils.PROGRAMOP_GLOBAL_BLOCK_POS).sons().get(Utils.GLOBAL_BLOCK_VAR_DECLS_LIST_POS);
            content.append(genGlobalVarsDecls(vardecls));
        }
        // Functions declarations
        for (VisitableNode function_def : root.sons().get(funcListPos).sons()) {
            content.append(genFunctionsDeclaration(function_def));
            content.append(";\n");
        }
        // Functions declaration global_vars_init
        content.append(_defGlobalVarsInit());

        // Functions implementations
        content.append("\n");
        for (VisitableNode function_def : root.sons().get(funcListPos).sons()) {
            // The accessory function to initialize the global vars is set as first action of the main
            String additional = (function_def.sons().get(Utils.FUNCALLOP_ID_POS).data().getSymClass()==Constants.MAIN ? "\t" + Constants.C_GLOBAL_VARS_INIT_CALL + Constants.C_SEMI : "") + "\n";
            content.append(genFunctionsDeclaration(function_def))
            .append(" {\n")
            .append(additional)
            .append(genBody(function_def.sons().size() == 4 ? function_def.sons().get(Utils.BODYOP_POS_PARAMS) : function_def.sons().get(Utils.BODYOP_POS_NO_PARAMS)))

            .append("\n}\n\n");
        }
        // An accessory function to eventually initialize the global variables
        content.append(_funcImplGlobalVarsInit(root.sons().get(Utils.PROGRAMOP_GLOBAL_BLOCK_POS).sons().get(Utils.GLOBAL_BLOCK_VAR_DECLS_LIST_POS)));
    }


    /**
     * It handles the declarations in the global block, not the initialization
     * @param var_decls_list A list of variable declarations VisitableNode
     */
    private String genGlobalVarsDecls(VisitableNode var_decls_list) throws CodeGenerationException {
        StringBuilder toReturn = new StringBuilder();
        for (VisitableNode var_decl : var_decls_list.sons()) {
            toReturn.append(genExpr(var_decl.sons().get(Utils.VAR_DECL_TYPE_POS)))
                    .append(" ")
                    .append(getAdjustedID(var_decl.sons().get(Utils.VAR_DECL_ID_POS)));
            int type_child = var_decl.sons().get(Utils.VAR_DECL_TYPE_POS).data().getSymClass();
            if (type_child == Constants.ARRAY_TYPE_SYM | type_child == Constants.STRING)
                toReturn.append(_defArrayTypeInC());

            toReturn.append(Constants.C_SEMI);
            toReturn.append("\n");
        }
        toReturn.append("\n\n");
        return toReturn.toString();
    }

    private String _defArrayTypeInC(){
        return Constants.C_ARRAY_DECL + Constants.C_ASSIGN + Constants.C_ARRAY_INIT;
    }

    private String _defInitStringInC(VisitableNode node) throws CodeGenerationException {
       return   Constants.C_STRCPY_FUN +
                Constants.C_LPAR +
                getAdjustedID(node.sons().get(Utils.VAR_DECL_ID_POS)) +
                Constants.C_COMMA +
                genExpr(node.sons().get(Utils.GLOBAL_BLOCK_ASSIGN_POS)) +
                Constants.C_RPAR;
    }


    /**
     * It handles local declarations with or without initialization translation to C
     * @param var_decls_list A list of variable declarations VisitableNode
     */
    private String genLocalVarsDecls(VisitableNode var_decls_list) throws CodeGenerationException {

        StringBuilder toReturn = new StringBuilder();
        for (VisitableNode var_decl : var_decls_list.sons()) {
            toReturn.append(genExpr(var_decl.sons().get(Utils.VAR_DECL_TYPE_POS)))
                    .append(" ")
                    .append(getAdjustedID(var_decl.sons().get(Utils.VAR_DECL_ID_POS)));
            int type_child = var_decl.sons().get(Utils.VAR_DECL_TYPE_POS).data().getSymClass();
            if (var_decl.sons().size() == 3){
                if(type_child == Constants.STRING){
                    toReturn.append(_defArrayTypeInC())
                            .append(Constants.C_SEMI)
                            .append("\n")
                            .append(_defInitStringInC(var_decl));
                }
                else
                    toReturn.append(Constants.C_ASSIGN).append(genExpr(var_decl.sons().get(Utils.VAR_DECL_EXPR_POS)));

            }
            if (type_child == Constants.ARRAY_TYPE_SYM)
                toReturn.append(_defArrayTypeInC());


            toReturn.append(Constants.C_SEMI)
                    .append("\n");
        }
        toReturn.append("\n\n");
        return toReturn.toString();
    }

    /**
     * It Handles the translation of the expressions and the types in C.
     * It uses recursion.
     * @param expr an Expression VisitableNode
     */
    private String genExpr(VisitableNode expr) throws CodeGenerationException {
        SymbolCustom data = expr.data();
        switch (data.getSymClass()) {
            case Constants.ID: return getAdjustedID(expr);
            case Constants.MAIN: return Constants.C_MAIN;
            case Constants.INT:
            case Constants.BOOL: return Constants.C_INT;
            case Constants.BOOL_CONST: return data.value.toString().equals(Constants.terminalNames[sym.FALSE]) ? Constants.C_FALSE.toString() : Constants.C_TRUE.toString();
            case Constants.FLOAT: return Constants.C_FLOAT;
            case Constants.FLOAT_CONST:
            case Constants.INT_CONST: return data.value.toString();
            case Constants.STRING_CONST: return "\"" + data.value.toString() + "\"";
            case Constants.STRING: return Constants.C_STRING;
            case Constants.ARRAY_TYPE_SYM: return genExpr(expr.sons().get(Utils.EXPR_FIRST_CHILD_POS));
            case Constants.NIL: return Constants.C_NIL;
            case Constants.PLUS: return genExpr(expr.sons().get(Utils.EXPR_FIRST_CHILD_POS)) + Constants.C_PLUS_OP + " " + genExpr(expr.sons().get(Utils.EXPR_SECOND_CHILD_POS));
            case Constants.MINUS: return expr.sons().size()==1 ? Constants.C_MINUS_OP + " " +  genExpr(expr.sons().get(Utils.EXPR_FIRST_CHILD_POS)) :  genExpr(expr.sons().get(Utils.EXPR_FIRST_CHILD_POS)) + Constants.C_MINUS_OP + " " + genExpr(expr.sons().get(Utils.EXPR_SECOND_CHILD_POS));
            case Constants.TIMES: return genExpr(expr.sons().get(Utils.EXPR_FIRST_CHILD_POS)) + Constants.C_TIMES_OP + " " + genExpr(expr.sons().get(Utils.EXPR_SECOND_CHILD_POS));
            case Constants.DIV: return Constants.C_FLOAT_CAST + genExpr(expr.sons().get(Utils.EXPR_FIRST_CHILD_POS)) + Constants.C_DIV_OP + Constants.C_FLOAT_CAST + genExpr(expr.sons().get(Utils.EXPR_SECOND_CHILD_POS));
            case Constants.AND: return genExpr(expr.sons().get(Utils.EXPR_FIRST_CHILD_POS)) + Constants.C_AND_OP + genExpr(expr.sons().get(Utils.EXPR_SECOND_CHILD_POS));
            case Constants.OR: return genExpr(expr.sons().get(Utils.EXPR_FIRST_CHILD_POS)) + Constants.C_OR_OP + genExpr(expr.sons().get(Utils.EXPR_SECOND_CHILD_POS));
            case Constants.EQ: return genExpr(expr.sons().get(Utils.EXPR_FIRST_CHILD_POS)) + Constants.C_EQ_OP + genExpr(expr.sons().get(Utils.EXPR_SECOND_CHILD_POS));
            case Constants.GT: return genExpr(expr.sons().get(Utils.EXPR_FIRST_CHILD_POS)) + Constants.C_GT_OP + genExpr(expr.sons().get(Utils.EXPR_SECOND_CHILD_POS));
            case Constants.GE: return genExpr(expr.sons().get(Utils.EXPR_FIRST_CHILD_POS)) + Constants.C_GE_OP + genExpr(expr.sons().get(Utils.EXPR_SECOND_CHILD_POS));
            case Constants.LE: return genExpr(expr.sons().get(Utils.EXPR_FIRST_CHILD_POS)) + Constants.C_LE_OP + genExpr(expr.sons().get(Utils.EXPR_SECOND_CHILD_POS));
            case Constants.LT: return genExpr(expr.sons().get(Utils.EXPR_FIRST_CHILD_POS)) + Constants.C_LT_OP + genExpr(expr.sons().get(Utils.EXPR_SECOND_CHILD_POS));
            case Constants.NOT: return Constants.C_NOT_OP + genExpr(expr.sons().get(0));
            case Constants.FUN_CALL_OP_SYM: return fun_call_op(expr);
            default: throw new CodeGenerationException("Unexpected Expr code: " + expr);
        }
    }


    /**
     * Generates the line of a function declaration in C
     * @param function_def A function definition VisitableNode
     */
    private String genFunctionsDeclaration(VisitableNode function_def) throws CodeGenerationException {
        StringBuilder toReturn = new StringBuilder();
        int returnTypePos;
        boolean isMain = function_def.sons().get(Utils.FUNCTION_DEF_ID_POS).data().value.toString().equals(Constants.C_MAIN.toUpperCase());
        if (function_def.sons().size() == 4) returnTypePos = Utils.FUNCTION_DEF_TYPE_POS_WITH_PARAMS;
        else returnTypePos = Utils.FUNCTION_DEF_TYPE_POS_NO_PARAMS;
        toReturn.append(isMain ? Constants.C_INT : genExpr(function_def.sons().get(returnTypePos)))
                .append(" ")
                .append(isMain ? Constants.C_MAIN : function_def.sons().get(Utils.FUNCTION_DEF_ID_POS).data().value)
                .append("(")
                .append(returnTypePos == Utils.FUNCTION_DEF_TYPE_POS_WITH_PARAMS ? genParametersDeclaration(function_def.sons().get(Utils.FUNCTION_DEF_PARDECLS_LIST_POS)) : "")
                .append(")");
        return toReturn.toString();
    }

    /**
     * Generates a list in C of parameters for the declaration of the functions
     * @param parDeclsList the list of Parameter declarations VisitableNode
     */
    private String genParametersDeclaration(VisitableNode parDeclsList) throws CodeGenerationException {
        StringBuilder toReturn = new StringBuilder();
        toReturn.append(genExpr(parDeclsList.sons().get(0).sons().get(1)))
                .append(" ")
                .append(genExpr(parDeclsList.sons().get(0).sons().get(0)));

        for (int i = 1; i < parDeclsList.sons().size(); i++) {
            toReturn.append(", ")
                    .append(genExpr(parDeclsList.sons().get(i).sons().get(1)))
                    .append(" ")
                    .append(genExpr(parDeclsList.sons().get(i).sons().get(0)));
        }
        return toReturn.toString();
    }


    /**
     * Get a body, that is a list of statements, and map each of them in succession
     * @param body list of statements VisitableNode
     * @return A String containing the statements translated in C
     */
    private String genBody(VisitableNode body) throws CodeGenerationException {
        StringBuilder toReturn = new StringBuilder();
        for(VisitableNode child: body.sons())
            toReturn.append("\t").append(mapStmt(child)).append("\n");
        return toReturn.toString();
    }

    private String _genReadOpListInC(VisitableNode ids_list){
        StringBuilder toReturn = new StringBuilder();
        for (VisitableNode id: ids_list.sons()){
            toReturn.append("\t")
                    .append(Constants.C_SCANF).append(Constants.C_LPAR)
                    .append(_mapFormatTypeInC(id.getType()))
                    .append(Constants.C_COMMA)
                    .append("&")
                    .append(getAdjustedID(id))
                    .append(Constants.C_RPAR)
                    .append(Constants.C_SEMI)
                    .append("\n");
        }

        return toReturn.toString();
    }

    private String _genWriteOpListInC(VisitableNode exprs_list) throws CodeGenerationException {
        StringBuilder toReturn = new StringBuilder();
        for (VisitableNode expr: exprs_list.sons()){
            int type = expr.getType();
            toReturn.append("\t")
                    .append(Constants.C_PRINTF).append(Constants.C_LPAR)
                    .append(_mapFormatTypeInC(type))
                    .append(Constants.C_COMMA)
                    .append(genExpr(expr))
                    .append(Constants.C_RPAR)
                    .append(Constants.C_SEMI)
                    .append("\n");
        }
        return toReturn.toString();
    }

    private String _mapFormatTypeInC(int type){
        String toReturn=null;
        if(type == Constants.INT || type == Constants.BOOL)
            toReturn="\"%d\"";
        else if(type == Constants.FLOAT)
            toReturn = "\"%f\"";
        else if(type == Constants.STRING)
            toReturn = "\"%s\"";
        return toReturn;
    }

    /**
     * Identifies the nature of a statement and translates it in C
     * @param stmt A statement VisitableNode
     * @return The statement string translated in C
     * @throws CodeGenerationException in case of a not expected/implemented code transformation
     */
    private String mapStmt(VisitableNode stmt) throws CodeGenerationException {
        StringBuilder toReturn = new StringBuilder();
        switch (stmt.data().getSymClass()) {
            case Constants.NOP_OP_SYM: return " " + Constants.C_SEMI;
            case Constants.ID_ASSIGNMENT_SYM:
                return getAdjustedID(stmt.sons().get(Utils.ID_ASSIGNMENT_VAR_POS)) + Constants.C_ASSIGN + genExpr(stmt.sons().get(Utils.ID_ASSIGNMENT_EXPR_POS)) +  Constants.C_SEMI;
            case Constants.FUN_CALL_OP_SYM: return fun_call_op(stmt) + Constants.C_SEMI;
            case Constants.WHILE_OP_SYM:
                toReturn.append(Constants.C_WHILE)
                        .append(Constants.C_LPAR)
                        .append(genExpr(stmt.sons().get(Utils.WHILE_COND_POS)))
                        .append(Constants.C_RPAR)
                        .append(" {\n")
                        .append(genBody(stmt.sons().get(Utils.WHILE_BODY_POS)))
                        .append("}");
                return toReturn.toString();
            case Constants.IF_OP_SYM:
                toReturn.append(Constants.C_IF)
                        .append(Constants.C_LPAR)
                        .append(genExpr(stmt.sons().get(Utils.IF_COND_POS)))
                        .append(Constants.C_RPAR)
                        .append(" {\n")
                        .append(genBody(stmt.sons().get(Utils.IF_BODY_POS)))
                        .append("}");
                return toReturn.toString();
            case Constants.IF_ELSE_OP_SYM:
                toReturn.append(Constants.C_IF)
                        .append(Constants.C_LPAR)
                        .append(genExpr(stmt.sons().get(Utils.IF_COND_POS)))
                        .append(Constants.C_RPAR)
                        .append(" {\n")
                        .append(genBody(stmt.sons().get(Utils.IF_BODY_POS)))
                        .append("}")
                        .append(Constants.C_ELSE)
                        .append(" {\n")
                        .append(genBody(stmt.sons().get(Utils.IF_ELSE_BODY_POS)))
                        .append("}");
                return toReturn.toString();
            case Constants.FOR_OP_SYM:
                toReturn.append(Constants.C_FOR)
                        .append(Constants.C_LPAR)
                        .append(Constants.C_INT)
                        .append(" ")
                        .append(mapStmt(stmt.sons().get(Utils.FOR_ID_POS)))
                        .append(genExpr(stmt.sons().get(Utils.FOR_COND_POS)))
                        .append(Constants.C_SEMI)
                        .append(getAdjustedID(stmt.sons().get(Utils.FOR_ID_POS).sons().get(0))).append("++")
                        .append(Constants.C_RPAR)
                        .append(" {\n")
                        .append(genBody(stmt.sons().get(Utils.FOR_BODY_POS)))
                        .append("}");
                return toReturn.toString();
            case Constants.RETURN_OP_SYM:
                toReturn.append(Constants.C_RETURN)
                        .append(" ")
                        .append(genExpr(stmt.sons().get(Utils.RETURN_BODY_POS)))
                        .append(Constants.C_SEMI);
                return toReturn.toString();
            case Constants.LOCAL_BLOCK_SYM:
                toReturn.append(genLocalVarsDecls(stmt.sons().get(Utils.LOCAL_BLOCK_VARDECLS_POS)))
                        .append(genBody(stmt.sons().get(Utils.LOCAL_BLOCK_BODY_POS)));

                return toReturn.toString();

            case Constants.READ_OP_SYM:
                return toReturn.append(_genReadOpListInC(stmt.sons().get(0))).toString();

            case Constants.WRITE_OP_SYM:
                return toReturn.append(_genWriteOpListInC(stmt.sons().get(0))).toString();

            default: return "Ancora non implementato";
        }
    }

    /**
     * Translates a function call of myPallene to a function call in C
     * @param funCall The function call VisitableNode
     * @return  A String of C of the function call
     */
    private String fun_call_op (VisitableNode funCall) throws CodeGenerationException {
        StringBuilder toReturn = new StringBuilder();
        toReturn    .append(funCall.sons().get(Utils.FUNCALLOP_ID_POS).data().value.toString())
                    .append(Constants.C_LPAR);
        //In case of arguments
        if (funCall.sons().size() == 2) {
            toReturn.append(genExpr(funCall.sons().get(Utils.FUNCALLOP_EXPRS_LIST).sons().get(0)));
            for (int i = 1; i < funCall.sons().get(Utils.FUNCALLOP_EXPRS_LIST).sons().size(); i++)
                toReturn.append(Constants.C_COMMA)
                        .append(genExpr(funCall.sons().get(Utils.FUNCALLOP_EXPRS_LIST).sons().get(i)));
        }
        toReturn.append(Constants.C_RPAR);

        return toReturn.toString();
    }

    private String _defGlobalVarsInit(){
        return Constants.C_VOID + " " + Constants.C_FUNC_GLOBAL_VARS_INIT + Constants.C_LPAR + Constants.C_RPAR + Constants.C_SEMI;
    }

    /**
     * The initialization of the global variables is done here via this function
     * @param varDeclsList the list of variables
     */
    private String _funcImplGlobalVarsInit(VisitableNode varDeclsList) throws CodeGenerationException {
        StringBuilder toReturn = new StringBuilder();
        toReturn.append(Constants.C_VOID).append(" ").append(Constants.C_GLOBAL_VARS_INIT_CALL).append("{" + "\n");
        for (VisitableNode var_decl : varDeclsList.sons()) {
            if(var_decl.sons().size()==3){
                toReturn.append("\t");
                if(var_decl.sons().get(Utils.VAR_DECL_TYPE_POS).data().getSymClass()==Constants.STRING)
                    toReturn.append(_defInitStringInC(var_decl));
                else
                    toReturn.append(getAdjustedID(var_decl.sons().get(Utils.VAR_DECL_ID_POS)))
                            .append(Constants.C_ASSIGN)
                            .append(genExpr(var_decl.sons().get(Utils.GLOBAL_BLOCK_ASSIGN_POS)));


                 toReturn.append(Constants.C_SEMI)
                         .append("\n");
            }

        }
        toReturn.append("}");
        return toReturn.toString();
    }

    /**
     * This is necessary to handle the difference of scope areas in C and MyPallene.
     * Every variable will be followed by the id of the nearest ElementsTable where the id is
     * By this way any conflicts should be avoided
     * @param id VisitableNode representing an ID
     * @return the adjusted name of the id
     */
    private String getAdjustedID (VisitableNode id){
        return id.data().value.toString() + "__" + id.getElement().getTable().getId();
    }

    /**
     * Set the standard headers of a C program
     */
    private String getHeaders(){
        return "#include <stdlib.h>\n#include <stdio.h>\n#include <string.h>\n\n";
    }

    /**
     *
     * @return the C code string
     */
    public String getC (){
        return this.content.toString();
    }
}