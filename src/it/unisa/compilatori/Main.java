package it.unisa.compilatori;

import it.unisa.compilatori.exceptions.CodeGenerationException;
import it.unisa.compilatori.exceptions.SemanticException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class Main {

    public static void main(String[] args) throws Exception {
        it.unisa.compilatori.Lexer lexer = new it.unisa.compilatori.Lexer(new FileReader( new File(args[0])));
	    it.unisa.compilatori.parser p = new it.unisa.compilatori.parser(lexer);
	    p.parse();
	    System.out.println(p.getRoot());


        Semantix sem = new Semantix();
        try {
            sem.semanticAnalyzer(p.getRoot());
        } catch (SemanticException e) {
            System.err.println(e.getMessage());
            return;
        }

        //System.out.println(p.getRoot().toStringTables());

        //Code generation
        CodeGenerator codeGenerator = null;
        try {
            codeGenerator = new CodeGenerator(p.getRoot(), lexer.symbolTable);
            codeGenerator.codeC();
            System.out.println(codeGenerator.getC());
            BufferedWriter writer = new BufferedWriter(new FileWriter("C/intermedio.c"));
            writer.write(codeGenerator.getC());
            writer.close();
        }
        catch (CodeGenerationException e){
            System.err.println(e.getMessage());
            System.out.println(codeGenerator.getC());
        }

    }
}
