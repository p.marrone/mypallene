package it.unisa.compilatori.visitor;


public interface Visitor {

    String visit(VisitableNode node);
}