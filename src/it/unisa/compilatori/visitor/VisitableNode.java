package it.unisa.compilatori.visitor;

import com.scalified.tree.TraversalAction;
import com.scalified.tree.TreeNode;
import com.scalified.tree.multinode.ArrayMultiTreeNode;
import it.unisa.compilatori.Element;
import it.unisa.compilatori.ElementsTable;
import it.unisa.compilatori.SymbolCustom;
import java.util.*;


public class VisitableNode extends ArrayMultiTreeNode<SymbolCustom> implements Visitable {

    private ElementsTable scope;
    private Element element;
    private int type;

    public VisitableNode(String data) {
       super(new SymbolCustom(-1, data));
    }
    public VisitableNode(SymbolCustom symbol){
        super(symbol);
    }

    @Override
    public java.lang.String accept(Visitor visitor) {
        return visitor.visit(this);
    }


    public Collection<? extends TreeNode<SymbolCustom>> subtrees() {
        if (isLeaf()) {
            return Collections.emptySet();
        }
        Collection<TreeNode<SymbolCustom>> subtrees = new ArrayList();
        for (int i = 0; i < super.subtrees().size(); i++) {
            TreeNode subtree = (TreeNode<SymbolCustom>) super.subtrees().toArray()[i];
            subtrees.add(subtree);
        }
        return subtrees;
    }

    public ArrayList<VisitableNode> sons() {
        ArrayList<VisitableNode> sons = new ArrayList<>();
        if (isLeaf())
            return sons;

        for (int i = 0; i < super.subtrees().size(); i++) {
            VisitableNode son = (VisitableNode) super.subtrees().toArray()[i];
            sons.add(son);
        }
        return sons;
    }


    public java.lang.String toStringTables() {
        final StringBuilder builder = new StringBuilder();
        builder.append("\n");
        final int topNodeLevel = level();
        TraversalAction<TreeNode<SymbolCustom>> action = new TraversalAction<TreeNode<SymbolCustom>>() {
            @Override
            public void perform(TreeNode<SymbolCustom> node) {
                int nodeLevel = node.level() - topNodeLevel;
                for (int i = 0; i < nodeLevel; i++) {
                    builder.append("|  ");
                }
                builder
                        .append("+- ")
                        .append(node.data())
                        .append(" -->\n")
                        .append(((VisitableNode)node).getScope()==null?"null":((VisitableNode)node).getScope().printLocalTable())
                        .append("\n");
            }

            @Override
            public boolean isCompleted() {
                return false;
            }
        };
        traversePreOrder(action);
        return builder.toString();
    }

    public ElementsTable getScope() {
        return scope;
    }


    public void setScope(ElementsTable scope) {
        this.scope = scope;
    }

    public void inheritScope(ElementsTable fatherTable) {
        if (this.isLeaf()) return;
        for (VisitableNode child: this.sons()) {
            if (child.getScope() == null) {
                child.setScope(fatherTable);
                child.inheritScope(fatherTable);
            }
            else
                child.inheritScope(child.getScope());

        }
    }

    public void setElement(Element element) { this.element = element; }
    public Element getElement() {return this.element; }
    public int getType(){ return this.type; }
    public void setType(int type) {this.type = type; }

}
