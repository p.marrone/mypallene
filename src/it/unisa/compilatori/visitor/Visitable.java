package it.unisa.compilatori.visitor;

public interface Visitable {
    String accept(Visitor visitor);
}