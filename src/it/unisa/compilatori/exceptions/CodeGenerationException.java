package it.unisa.compilatori.exceptions;

public class CodeGenerationException extends Throwable {
    public CodeGenerationException(String message) {
        super(message);
    }
}
