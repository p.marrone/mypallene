package it.unisa.compilatori.exceptions;

public class SemanticException extends Throwable {
    public SemanticException(String message) {
        super(message);
    }
}
