package it.unisa.compilatori;

import it.unisa.compilatori.exceptions.SemanticException;

import java.util.HashMap;
import java.util.Map;

public class ElementsTable {

    private ElementsTable father;
    HashMap<String, Element> localTable;
    private static int generations = 0;
    private int id;

    public ElementsTable(){
        localTable = new HashMap<>();
        father = null;
        id = generations++;
    }

    public ElementsTable(ElementsTable father){
        localTable = new HashMap<>();
        this.father = father;
        id = generations++;
    }

    public void installElement(Element e) throws SemanticException {
        if (lookupLocally(e.getId()) == null) {
            e.setTable(this);
            localTable.put(e.getId(), e);
        }
        else
            throw new SemanticException( e + "\nGià presente nella scope table:\n" + this.printLocalTable());
    }

    public Element lookup(String id){
        Element e = lookupLocally(id);
        if (e == null)
            if (father != null)
                return father.lookup(id);
        return e;
    }

    public Element lookupLocally(String id){
        return localTable.get(id);
    }

    public int getId() {return id;}

    public String printLocalTable(){
        StringBuilder toPrint = new StringBuilder("\t\t\t\t\t\t\t\t\t\t\t\t| kind |  id  | type |\n");
        for(Map.Entry<String, Element> entry : localTable.entrySet()) {
            Element element = entry.getValue();
            toPrint.append("\t\t\t\t\t\t\t\t\t\t\t\t| ").append(element.getKind()).append(" | ").append(element.getId()).append(" | ").append(element.getType()).append(" |\n");
        }
        return toPrint.toString();
    }

}
