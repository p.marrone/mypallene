package it.unisa.compilatori;

import it.unisa.compilatori.exceptions.SemanticException;
import it.unisa.compilatori.visitor.VisitableNode;

import java.util.ArrayList;



public class Semantix {
    boolean existsMain;
    TypeSystem typeSystem;

    int currentFunctionReturnType;

    public Semantix() {
        existsMain = false;
        typeSystem = new TypeSystem();
        currentFunctionReturnType = -1;
    }

    /**
     * Two things are made here:
     *  - Constructs and appends to the tree nodes the scope tables
     *  - Checks for syntactic sanity like:
     *  |-- Proper declaration of variables before the use
     *  |-- Proper function calls: number of parameters and their types
     *  |-- Type check on assignments
     *  |-- Type check (boolean) on while, if, and for conditions
     * @param root The root node of the syntax tree
     * @throws SemanticException in case of one of the above checks fails
     */
    public void semanticAnalyzer(VisitableNode root) throws SemanticException {
        int posFuncs = root.sons().get(Utils.PROGRAMOP_GLOBAL_BLOCK_POS).data().getSymClass() == Constants.GLOBAL_BLOCK_SYM ? Utils.PROGRAMOP_FUNC_LIST_POS : 0;

        ElementsTable rootTable = new ElementsTable();
        // Fulfilling the Root Scope Table with the names of the functions
        populateFunctions(rootTable, root.sons().get(posFuncs));

        // Fulfilling the Root Scope Table with the elements in the Global Block (if it exists)
        if (root.sons().get(Utils.PROGRAMOP_GLOBAL_BLOCK_POS).data().getSymClass() ==  Constants.GLOBAL_BLOCK_SYM)
            populateVars(rootTable, root.sons().get(Utils.PROGRAMOP_GLOBAL_BLOCK_POS).sons().get(Utils.GLOBAL_BLOCK_VAR_DECLS_LIST_POS));


        // Each function needs its own Scope Table
        for (VisitableNode function:root.sons().get(posFuncs).sons()) {
            ElementsTable functionScope = new ElementsTable(rootTable);
            // Fulfilling the new function scope table with the parameters (if any)
            populateParams(functionScope, function.sons().get(Utils.FUNCTION_DEF_PARDECLS_LIST_POS));


            int bodyPos;
            // The body position and current function return type changes according to presence or absence of the function parameters
            if(function.sons().size()==4){
                currentFunctionReturnType = function.sons().get(Utils.FUNCTION_DEF_TYPE_POS_WITH_PARAMS).data().getSymClass();
                bodyPos = 3;
            }
            else{
                currentFunctionReturnType = function.sons().get(Utils.FUNCTION_DEF_TYPE_POS_NO_PARAMS).data().getSymClass();
                bodyPos = 2;
            }
            // Here we recursively need to look for LOCAL and FOR nodes and create a scope table of each of them
            genBodyScopeTables(functionScope, function.sons().get(bodyPos));

            function.setScope(functionScope);
        }
        //root.inheritScope(rootTable);
        root.setScope(rootTable);
    }


    /**
     * Get the Declared Variables whose list node is given in input and insert them in the scope table given in input.
     * It also checks if the assignment to the new declared variable contains no not-declared variables
     * @param table The scope table where to add the variables
     * @param varDeclsList The list/father node (VAR_DECLS_LIST) of the VAR_DECLs
     * @throws SemanticException In case of attempt to assign a not yet declared variable to a new variable
     */
    private void populateVars(ElementsTable table, VisitableNode varDeclsList ) throws SemanticException {
        for (VisitableNode child:varDeclsList.sons()) {
            // Check sanity of assignment
            if (child.sons().size() == Utils.VAR_DECL_SIZE_WITH_ASSIGN) {
                child.setScope(table);
                child.inheritScope(table);
                typeSystem.checkType(child.sons().get(Utils.VAR_DECL_TYPE_POS).data().getSymClass(), child.sons().get(Utils.VAR_DECL_EXPR_POS));
            }
            Element e = new Element(Constants.VARIABLE_KIND, child.sons().get(Utils.VAR_DECL_ID_POS).data().value.toString(), child.sons().get(Utils.VAR_DECL_TYPE_POS).data().getSymClass());
            child.sons().get(Utils.VAR_DECL_ID_POS).setElement(e);
            table.installElement(e);
        }
        varDeclsList.setScope(table);
    }

    /**
     * Get the function parameters whose list node is given in input and insert them in the scope table given in input
     * @param table The scope table where to add the variables
     * @param parDeclsList The list/father node (PAR_DECLS_LIST) of the PAR_DECLs
     */
    private void populateParams(ElementsTable table, VisitableNode parDeclsList ) throws SemanticException {
        for (VisitableNode par_decl:parDeclsList.sons()) {
            Element e = new Element(Constants.VARIABLE_KIND, par_decl.sons().get(Utils.PAR_DECL_ID_POS).data().value.toString(), par_decl.sons().get(Utils.PAR_DECL_TYPE_POS).data().getSymClass());
            par_decl.sons().get(Utils.PAR_DECL_ID_POS).setElement(e);
            table.installElement(e);
        }
        parDeclsList.setScope(table);
    }


    /**
     * Gets the functions' names and insert them as "fun" kind in the scope table given in input
     * and checks the uniqueness of the main
     * @param table Scope table where the functions' names need to be added
     * @param functionsList The nodes whose sons are the "function" elements
     * @throws SemanticException In case of presence of more than one main
     */
    private void populateFunctions(ElementsTable table, VisitableNode functionsList  ) throws SemanticException {

        for(VisitableNode function:functionsList.sons()) {
            // Getting the name
            SymbolCustom functionSymbol = function.sons().get(Utils.FUNCTION_DEF_ID_POS).data();
            String functionName;

            // Checking the uniqueness of main
            if (functionSymbol.getSymClass() == Constants.MAIN) {
                if (existsMain)
                    throw new SemanticException("It exists more than one main");
                existsMain = true;
            }
            functionName = functionSymbol.value.toString();

            // The position of the return type is 1 if the function takes no parameters, 2 otherwise
            if (function.sons().size() == 3)
                table.installElement(new Element(Constants.FUNCTION_KIND, functionName, function.sons().get(Utils.FUNCTION_DEF_TYPE_POS_NO_PARAMS).data().getSymClass()));
            else {
                Element ele = new Element(Constants.FUNCTION_KIND, functionName, function.sons().get(Utils.FUNCTION_DEF_TYPE_POS_WITH_PARAMS).data().getSymClass());
                // Let's insert in the table the parameters types for future easy checking
                ArrayList<Integer> paramsTypes = new ArrayList<>();
                for (VisitableNode par_decl : function.sons().get(Utils.FUNCTION_DEF_PARDECLS_LIST_POS).sons())
                    paramsTypes.add(par_decl.sons().get(Utils.PAR_DECL_TYPE_POS).data().getSymClass());
                ele.setParamsTypes(paramsTypes);
                table.installElement(ele);
            }
        }
        functionsList.setScope(table);
    }


    /**
     * Looks for LOCAL_BLOCKs or FOR blocks in all the tree whose root is given in input,
     * creates for each of them a local scope table fulfilled with their declared variables
     * and make all the sons inherit the nearest scope table.
     * The method is recursive
     * @param fatherTable The scope table to be set in case of non LOCAL or FOR node
     * @param node The node to be examined
     */
    private void genBodyScopeTables(ElementsTable fatherTable, VisitableNode node) throws SemanticException {
        if (node.isLeaf()) {
            node.setScope(fatherTable);
            return;
        }

        SymbolCustom symbol = node.data();
        ElementsTable localTable;
        switch (symbol.getSymClass()) {
            case Constants.LOCAL_BLOCK_SYM:
                localTable = new ElementsTable(fatherTable);
                populateVars(localTable, node.sons().get(Utils.LOCAL_BLOCK_VARDECLS_POS));
                node.setScope(localTable);
                for (VisitableNode child: node.sons().get(Utils.LOCAL_BLOCK_BODY_POS).sons())
                    genBodyScopeTables(localTable, child);
                return;
            case Constants.FOR_OP_SYM:
                localTable = new ElementsTable(fatherTable);
                Element e = new Element(Constants.VARIABLE_KIND, node.sons().get(Utils.FOR_ID_POS).sons().get(Utils.ID_ASSIGNMENT_VAR_POS).data().value.toString(), Constants.INT);
                node.sons().get(Utils.FOR_ID_POS).sons().get(Utils.ID_ASSIGNMENT_VAR_POS).setElement(e);
                localTable.installElement(e);
                node.setScope(localTable);
                node.sons().get(Utils.FOR_COND_POS).setScope(localTable);
                node.sons().get(Utils.FOR_COND_POS).inheritScope(localTable);
                typeSystem.checkType(Constants.BOOL, node.sons().get(Utils.FOR_COND_POS));
                for (VisitableNode child: node.sons().get(Utils.FOR_BODY_POS).sons())
                    genBodyScopeTables(localTable, child);
                return;
            case Constants.WHILE_OP_SYM:
                node.setScope(fatherTable);
                node.sons().get(Utils.WHILE_COND_POS).setScope(fatherTable);
                node.sons().get(Utils.WHILE_COND_POS).inheritScope(fatherTable);
                typeSystem.checkType(Constants.BOOL, node.sons().get(Utils.WHILE_COND_POS));
                for (VisitableNode child: node.sons().get(Utils.WHILE_BODY_POS).sons())
                    genBodyScopeTables(fatherTable, child);
                return;
            case Constants.IF_OP_SYM:
                node.setScope(fatherTable);
                node.sons().get(Utils.IF_COND_POS).setScope(fatherTable);
                node.sons().get(Utils.IF_COND_POS).inheritScope(fatherTable);
                typeSystem.checkType(Constants.BOOL, node.sons().get(Utils.IF_COND_POS));
                for (VisitableNode child: node.sons().get(Utils.IF_BODY_POS).sons())
                    genBodyScopeTables(fatherTable, child);
                return;
            case Constants.IF_ELSE_OP_SYM:
                node.setScope(fatherTable);
                node.sons().get(Utils.IF_COND_POS).setScope(fatherTable);
                node.sons().get(Utils.IF_COND_POS).inheritScope(fatherTable);
                typeSystem.checkType(Constants.BOOL, node.sons().get(Utils.IF_COND_POS));
                for (VisitableNode child: node.sons().get(Utils.IF_BODY_POS).sons())
                    genBodyScopeTables(fatherTable, child);
                for (VisitableNode child: node.sons().get(Utils.IF_ELSE_BODY_POS).sons())
                    genBodyScopeTables(fatherTable, child);
                return;
            case Constants.ID_ASSIGNMENT_SYM:
                node.setScope(fatherTable);
                node.inheritScope(fatherTable);
                typeSystem.checkAssignmentType(node.sons().get(Utils.ID_ASSIGNMENT_VAR_POS), node.sons().get(Utils.ID_ASSIGNMENT_EXPR_POS));
                return;
            case Constants.FUN_CALL_OP_SYM:
                node.setScope(fatherTable);
                node.inheritScope(fatherTable);
                if (node.sons().size() == 2)
                    typeSystem.checkParamsType(fatherTable.lookup(node.sons().get(Utils.FUNCALLOP_ID_POS).data().value.toString()), node.sons().get(Utils.FUNCALLOP_EXPRS_LIST));
                return;
            case Constants.RETURN_OP_SYM:
                node.setScope(fatherTable);
                node.inheritScope(fatherTable);
                typeSystem.checkType(currentFunctionReturnType, node.sons().get(Utils.RETURN_BODY_POS));
                return;
            case Constants.WRITE_OP_SYM:
            case Constants.READ_OP_SYM:
                node.setScope(fatherTable);
                node.inheritScope(fatherTable);
                for (VisitableNode ex : node.sons().get(Utils.READ_OP_IDs_LIST_POS).sons())
                    typeSystem.getExprType(ex);
                return;
        }
        node.setScope(fatherTable);
        for (VisitableNode child: node.sons())
            genBodyScopeTables(fatherTable, child);

    }

}
