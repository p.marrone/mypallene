package it.unisa.compilatori;
public class Constants extends sym {
    public static final int     ARRAY_TYPE_SYM = 70,
                                FUNCTION_TYPE_SYM = 71,
                                FUN_CALL_OP_SYM = 72,
                                ID_ASSIGNMENT_SYM = 73,
                                VARIABLE_KIND = 74,
                                FUNCTION_KIND = 75,
                                LOCAL_BLOCK_SYM = 76,
                                BODY_OP_SYM = 77,
                                RETURN_OP_SYM = 78,
                                READ_OP_SYM = 79,
                                IDs_LIST_SYM = 80,
                                EXPRS_LIST_SYM = 81,
                                FOR_OP_SYM = 82,
                                NOP_OP_SYM = 83,
                                WRITE_OP_SYM = 84,
                                VAR_DECL_SYM = 85,
                                VAR_DECLS_LIST_SYM = 86,
                                WHILE_OP_SYM = 87,
                                IF_OP_SYM = 88,
                                IF_ELSE_OP_SYM = 89,
                                GLOBAL_BLOCK_SYM = 90;

    public static final String  PROGRAMMA = "PROGRAMMA",
                                GLOBAL_BLOCK = "GLOBAL_BLOCK",
                                FUNCTION_LIST = "FUNCTIONS_LIST",
                                FUNCTION_DEF = "FUNCTION_DEF",
                                PAR_DECLS_LIST = "PAR_DECLS_LIST",
                                PAR_DECL = "PAR_DECL",
                                VAR_DECL = "VAR_DECL",
                                VAR_DECLS_LIST = "VAR_DECLS_LIST",
                                ARRAY_TYPE = "ARRAY_TYPE",
                                FUNCTION_TYPE = "FUNCTION_TYPE",
                                TYPES_LIST = "TYPES_LIST",
                                BODY_OP = "BODY_OP",
                                NOP_OP ="NOP_OP",
                                WHILE = "WHILE",
                                IF_THEN = "IF_THEN",
                                IF_THEN_ELSE = "IF_THEN_ELSE",
                                FOR_OP = "FOR_OP",
                                WHILE_OP = "WHILE_OP",
                                IF_OP = "IF_OP",
                                IF_ELSE_OP = "IF_ELSE_OP",
                                ID_ASSIGNMENT = "ID_ASSIGNMENT",
                                LOCAL_BLOCK ="LOCAL_BLOCK",
                                ARRAY_WRITE ="ARRAY_WRITE",
                                FUN_CALL_OP = "FUN_CALL_OP",
                                READ_OP = "READ_OP",
                                WRITE_OP = "WRITE_OP",
                                RETURN_OP = "RETURN_OP",
                                IDs_LIST = "IDs_LIST",
                                EXPRS_LIST = "EXPRS_LIST",
                                ARRAY_EMPTY = "ARRAY_EMPTY",
                                ARRAY_READ = "ARRAY_READ",
                                PLUS_OP = "PLUS_OP",
                                MINUS_OP = "MINUS_OP",
                                TIMES_OP = "TIMES_OP",
                                DIV_OP = "DIV_OP",
                                AND_RELOP = "AND_RELOP",
                                OR_RELOP = "OR_RELOP",
                                GREAT_THAN_RELOP = "GREAT_THAN_RELOP",
                                GREAT_EQUAL_RELOP = "GREAT_EQUAL_RELOP",
                                LESS_THAN_RELOP = "LESS_THAN_RELOP",
                                LESS_EQUAL_RELOP = "LESS_EQUAL_RELOP",
                                EQUAL_RELOP = "EQUAL_RELOP",
                                NOT_EQUAL_RELOP = "NOT_EQUAL_RELOP",
                                NOT_NODE ="NOT_NODE",
                                SHARP_NODE ="SHARP_NODE",
                                UMINUS ="UMINUS";



    //MAPPING IN C
    public static final String  C_INT = "int",
                                C_FLOAT = "float",
                                C_STRING = "char";

    public static final String C_NIL = "void";
    public static final String C_MAIN = "main";
    public static final Character C_MINUS_OP = '-';
    public static final Character C_TIMES_OP = '*';
    public static final Character C_DIV_OP = '/';
    public static final String C_AND_OP = "&&";
    public static final String C_OR_OP = "||";
    public static final Character C_NOT_OP = '!';
    public static final String C_EQ_OP = "==";
    public static final Character C_GT_OP = '>';
    public static final String C_GE_OP = ">=";
    public static final String C_LE_OP = "<=";
    public static final Character C_LT_OP = '<';
    public static final Character C_ASSIGN = '=';
    public static final Character C_FALSE = '0';
    public static final Character C_TRUE = '1';
    public static final String C_ARRAY_DECL = "[]";
    public static final String C_ARRAY_INIT = "{}";
    public static final Character C_PLUS_OP='+';
    public static final Character C_SEMI=';';
    public static final Character C_LPAR='(';
    public static final Character C_RPAR=')';
    public static final Character C_COMMA=',';
    public static final String  C_WHILE = "while",
                                C_IF = "if",
                                C_ELSE = "else",
                                C_FOR = "for",
                                C_RETURN = "return",
                                C_VOID="void",
                                C_FUNC_GLOBAL_VARS_INIT="global_vars_init",
                                C_GLOBAL_VARS_INIT_CALL="global_vars_init()",
                                C_STRCPY_FUN="strcpy",
                                C_SCANF="scanf",
                                C_PRINTF="printf";
    public static final String  C_FLOAT_CAST="(float)";




}