package it.unisa.compilatori;

import java_cup.runtime.Symbol;

public class SymbolCustom extends Symbol {

    private String name;

    public SymbolCustom(int id,String name, int line, int column, Object o) {
        super(id, line, column, o);
        this.name = name;
    }

    public SymbolCustom(int id, String name, Object o) {
        super(id,o);
        this.name = name;
    }

    public SymbolCustom(int id,String name, int line, int column) {
        super(id, line, column);
        this.name = name;
    }

    public SymbolCustom(int id, String name){
        super(id);
        this.name = name;
    }

    public int getSymClass(){
        return super.sym;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String toString() {
        if (this.value != null)
            return  this.name.equals("STRING_CONST")? "<" + name + ", " +  '"' +this.value.toString() + '"' + ">" : "<" + name + ", "  + this.value.toString() + ">";
        else
            return "<" + name + ">";
    }

    public String toString2(){
        if(this.value!=null)
            return  this.name.equals("STRING_CONST")?   name + ", " +  '"' +this.value.toString() + '"' : name + ", "  + this.value.toString() ;
        else
            return name ;
    }
}
