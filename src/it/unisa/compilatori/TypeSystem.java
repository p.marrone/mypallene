package it.unisa.compilatori;

import it.unisa.compilatori.exceptions.SemanticException;
import it.unisa.compilatori.visitor.VisitableNode;

import java.util.ArrayList;
import java.util.HashMap;

public class TypeSystem {

    /*
     * The types operations tables are implemented as HashMaps where
     * the key is the concatenation of the type(s) and operation type,
     * the value is the type expected from the operation with such types in input
     */
    HashMap<String, Integer> one;
    HashMap<String, Integer> two;

    public TypeSystem(){
        fillOneOpMatrix();
        fillTwoOperatorsMatrix();
    }

    /**
     * Init of op table with 1 parameter input
     */
    private void fillOneOpMatrix(){

        one = new HashMap<>();
        one.put("" + Constants.INT + Constants.MINUS, Constants.INT);
        one.put("" + Constants.FLOAT + Constants.MINUS, Constants.FLOAT);
        one.put("" + Constants.BOOL + Constants.NOT, Constants.BOOL);
        one.put("" + Constants.ARRAY_TYPE_SYM + Constants.SHARP, Constants.INT);
        one.put("" + Constants.STRING + Constants.SHARP, Constants.INT);

    }

    /**
     * Init of op table with 2 parameters input
     */
    private void fillTwoOperatorsMatrix(){
        two = new HashMap<>();

        two.put(""+ Constants.INT + Constants.INT + Constants.PLUS, Constants.INT );
        two.put(""+ Constants.INT + Constants.INT + Constants.MINUS, Constants.INT );
        two.put(""+ Constants.INT + Constants.INT + Constants.TIMES, Constants.INT );
        two.put(""+ Constants.INT + Constants.INT + Constants.DIV, Constants.FLOAT );

        two.put(""+ Constants.INT + Constants.FLOAT + Constants.PLUS, Constants.FLOAT );
        two.put(""+ Constants.INT + Constants.FLOAT + Constants.MINUS, Constants.FLOAT );
        two.put(""+ Constants.INT + Constants.FLOAT + Constants.TIMES, Constants.FLOAT );
        two.put(""+ Constants.INT + Constants.FLOAT + Constants.DIV, Constants.FLOAT );

        two.put(""+ Constants.FLOAT + Constants.INT + Constants.PLUS, Constants.FLOAT );
        two.put(""+ Constants.FLOAT + Constants.INT + Constants.MINUS, Constants.FLOAT );
        two.put(""+ Constants.FLOAT + Constants.INT + Constants.TIMES, Constants.FLOAT );
        two.put(""+ Constants.FLOAT + Constants.INT + Constants.DIV, Constants.FLOAT );

        two.put(""+ Constants.FLOAT + Constants.FLOAT + Constants.PLUS, Constants.FLOAT );
        two.put(""+ Constants.FLOAT + Constants.FLOAT + Constants.MINUS, Constants.FLOAT );
        two.put(""+ Constants.FLOAT + Constants.FLOAT + Constants.TIMES, Constants.FLOAT );
        two.put(""+ Constants.FLOAT + Constants.FLOAT + Constants.DIV, Constants.FLOAT );

        two.put(""+ Constants.BOOL + Constants.BOOL + Constants.AND, Constants.BOOL );
        two.put(""+ Constants.BOOL + Constants.BOOL + Constants.OR, Constants.BOOL );

        two.put(""+ Constants.BOOL_CONST + Constants.BOOL_CONST + Constants.LT, Constants.BOOL );
        two.put(""+ Constants.BOOL_CONST + Constants.BOOL_CONST + Constants.LE, Constants.BOOL );
        two.put(""+ Constants.BOOL_CONST + Constants.BOOL_CONST + Constants.GE, Constants.BOOL );
        two.put(""+ Constants.BOOL_CONST + Constants.BOOL_CONST + Constants.GT, Constants.BOOL );
        two.put(""+ Constants.BOOL_CONST + Constants.BOOL_CONST + Constants.EQ, Constants.BOOL );

        two.put(""+ Constants.INT + Constants.INT + Constants.LT, Constants.BOOL );
        two.put(""+ Constants.INT + Constants.INT + Constants.LE, Constants.BOOL );
        two.put(""+ Constants.INT + Constants.INT + Constants.GT, Constants.BOOL );
        two.put(""+ Constants.INT + Constants.INT + Constants.GE, Constants.BOOL );
        two.put(""+ Constants.INT + Constants.INT + Constants.EQ, Constants.BOOL );

        two.put(""+ Constants.INT + Constants.FLOAT + Constants.LT, Constants.BOOL );
        two.put(""+ Constants.INT + Constants.FLOAT + Constants.LE, Constants.BOOL );
        two.put(""+ Constants.INT + Constants.FLOAT + Constants.GT, Constants.BOOL );
        two.put(""+ Constants.INT + Constants.FLOAT + Constants.GE, Constants.BOOL );
        two.put(""+ Constants.INT + Constants.FLOAT + Constants.EQ, Constants.BOOL );

        two.put(""+ Constants.FLOAT + Constants.INT + Constants.LT, Constants.BOOL );
        two.put(""+ Constants.FLOAT + Constants.INT + Constants.LE, Constants.BOOL );
        two.put(""+ Constants.FLOAT + Constants.INT + Constants.GT, Constants.BOOL );
        two.put(""+ Constants.FLOAT + Constants.INT + Constants.GE, Constants.BOOL );
        two.put(""+ Constants.FLOAT + Constants.INT + Constants.EQ, Constants.BOOL );

        two.put(""+ Constants.FLOAT + Constants.FLOAT + Constants.LT, Constants.BOOL );
        two.put(""+ Constants.FLOAT + Constants.FLOAT + Constants.LE, Constants.BOOL );
        two.put(""+ Constants.FLOAT + Constants.FLOAT + Constants.GT, Constants.BOOL );
        two.put(""+ Constants.FLOAT + Constants.FLOAT + Constants.GE, Constants.BOOL );
        two.put(""+ Constants.FLOAT + Constants.FLOAT + Constants.EQ, Constants.BOOL );

        two.put(""+ Constants.STRING + Constants.STRING + Constants.LT, Constants.BOOL );
        two.put(""+ Constants.STRING + Constants.STRING + Constants.LE, Constants.BOOL );
        two.put(""+ Constants.STRING + Constants.STRING + Constants.GT, Constants.BOOL );
        two.put(""+ Constants.STRING + Constants.STRING + Constants.GE, Constants.BOOL );
        two.put(""+ Constants.STRING + Constants.STRING + Constants.EQ, Constants.BOOL );

    }


    /**
     * Questions the op table 2
     * @return the expected type id
     * @throws SemanticException in case the given types and op are not combinable
     */
    private Integer calcType(int type1, int type2, int op) throws SemanticException {
        Integer id = two.get("" + type1 + type2 + op);
        if (id != null)
            return id;
        else
            throw new SemanticException("Cannot apply operator to the given types");
    }

    /**
     * Questions the op table 1
     * @return the expected type id
     * @throws SemanticException in case the given type and op are not combinable
     */
    private Integer calcType(int type, int op) throws SemanticException {
        Integer id = one.get("" + type + op);
        if (id != null)
            return id;
        else
            throw new SemanticException("Cannot apply operator to the given type");
    }


    /**
     * Calculates the type coming out of an entire expression.
     * In the meanwhile it also checks:
     *  - if the variables used are proper and declared
     *  - if the functions used are declared and if the arguments match the expected parameters
     *  - misuses of id of variable as for functions and viceversa
     *  - type checks for standard operations
     * @param expr Expression VisitableNode
     * @return the type of the expression
     * @throws SemanticException in the cases stated above
     */
    public int getExprType(VisitableNode expr) throws SemanticException {
        SymbolCustom symbol = expr.data();
        int type;
        switch (symbol.getSymClass()) {
            case Constants.INT_CONST: type =  Constants.INT; break;
            case Constants.FLOAT_CONST: type = Constants.FLOAT; break;
            case Constants.STRING_CONST: type = Constants.STRING; break;
            case Constants.BOOL_CONST: type = Constants.BOOL; break;
            case Constants.ARRAY_TYPE_SYM: type = Constants.ARRAY_TYPE_SYM; break;
            case Constants.ID:
                Element line = expr.getScope().lookup(symbol.value.toString());
                if (line == null)
                    throw new SemanticException("Variable not declared: \"" + symbol.value.toString() + "\"");
                if (!(line.getKind() == Constants.VARIABLE_KIND))
                    throw new SemanticException("\""+symbol.value.toString() + "\" is an incorrect function call, argument parameters are needed");
                expr.setElement(line);
                type = line.getType();
                break;
            case Constants.FUN_CALL_OP_SYM:
                Element line2 = expr.getScope().lookup(expr.sons().get(0).data().value.toString());
                if (line2 == null)
                    throw new SemanticException("Function not declared :" + symbol.value.toString());
                if (line2.getKind() == Constants.VARIABLE_KIND)
                    throw new SemanticException("\""+symbol.value.toString() + "\" is a variable, not a function");
                if (expr.sons().size() == 2)
                    checkParamsType(line2, expr.sons().get(1));
                type = line2.getType();
                break;
            default:
                if (expr.sons().size() == 1)
                    type = calcType(getExprType(expr.sons().get(0)), symbol.getSymClass());
                else if(expr.sons().size()==2)
                    type = calcType(getExprType(expr.sons().get(0)), getExprType(expr.sons().get(1)), symbol.getSymClass());
                else
                    throw new SemanticException("Unexpected Operation");
                break;
        }
        expr.setType(type);
        return type;

    }

    /**
     * Check if the type of the expr in input matches the type given in input
     * @throws SemanticException in case types are different
     */
    public void checkType (int type, VisitableNode node) throws SemanticException {
        int nodeType = getExprType(node);
        if (type != nodeType)
            throw new SemanticException("Not compatible types: " + type + ", " + nodeType);

    }

    /**
     * Checks whether the type of the left part of an assignment (the id) meets the type of the assignment expression (right part)
     * @throws SemanticException in case of not compatible types or id not declared
     */
    public void checkAssignmentType(VisitableNode id, VisitableNode ass) throws SemanticException {
        Element ele = id.getScope().lookup(id.data().value.toString());
        if (ele == null)
            throw new SemanticException("Variable not declared: \"" + id.data().value.toString() + "\"");
        int type2 = getExprType(ass);
        if (ele.getType() != type2)
            throw new SemanticException("Not compatible types: \"" + ele.getType() + "\" and \"" + type2 + "\"");
        id.setElement(ele);
    }

    /**
     * Checks if function call are used properly:
     *  - Declaration of the function
     *  - Number of arguments
     *  - Types of arguments given and expected
     * @param row the element in the elementsTable (scope) referring to the function; it has a the list of the types of the parameters
     * @param exprs_list The list of expressions passed to the function
     * @throws SemanticException in the case stated above
     */
    public void checkParamsType(Element row, VisitableNode exprs_list) throws SemanticException {
        if (row == null)
            throw new SemanticException("Function not declared, probably misspelled");
        ArrayList<Integer> types = row.getParamsTypes();
        if (types.size() != exprs_list.sons().size())
            throw new SemanticException("Wrong number of parameters: the function seeks for " + types.size() + " while " + exprs_list.sons().size() + " have been passed");
        for (int i = 0; i < types.size(); i++) {
            int expr_type = getExprType(exprs_list.sons().get(i));
            if (types.get(i) != expr_type)
                throw new SemanticException("Unexpected argument type at parameter " + (i + 1) + "; Expected: " + types.get(i) + ", but got: " + expr_type);
        }
    }
}
