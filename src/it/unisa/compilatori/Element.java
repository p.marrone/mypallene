package it.unisa.compilatori;

import java.util.ArrayList;

public class Element {

    private int kind;
    private int type;
    private String id;
    private ArrayList<Integer> paramsTypes;
    private ElementsTable table;

    public Element(int kind, String id, int type) {
        this.kind = kind;
        this.type = type;
        this.id = id;
        if (kind == Constants.FUNCTION_KIND)
            paramsTypes = new ArrayList<>();
        else
            paramsTypes = null;
    }


    public int getKind() { return kind; }

    public int getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public ArrayList<Integer> getParamsTypes() { return paramsTypes; }

    public void setParamsTypes(ArrayList<Integer> paramsTypes) { this.paramsTypes = paramsTypes; }

    public ElementsTable getTable() {
        return table;
    }

    public void setTable(ElementsTable table) {
        this.table = table;
    }

    @Override
    public String toString() {
        return "Element{kind='" + kind + '\'' + ", type='" + type + '\'' + ", id='" + id + '\'' + '}';
    }
}
