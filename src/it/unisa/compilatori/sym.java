
//----------------------------------------------------
// The following code was generated by CUP v0.11b 20160615 (GIT 4ac7450)
//----------------------------------------------------

package it.unisa.compilatori;

/** CUP generated class containing symbol constants. */
public class sym {
  /* terminals */
  public static final int FUNCTION = 4;
  public static final int GE = 45;
  public static final int UMINUS = 2;
  public static final int SLPAR = 30;
  public static final int SEMI = 11;
  public static final int INT = 15;
  public static final int MINUS = 39;
  public static final int FOR = 28;
  public static final int BRPAR = 20;
  public static final int WRITE = 33;
  public static final int NOT = 49;
  public static final int AND = 42;
  public static final int LT = 46;
  public static final int RPAR = 7;
  public static final int NIL = 13;
  public static final int NOP = 22;
  public static final int OR = 43;
  public static final int BOOL = 16;
  public static final int COMMA = 12;
  public static final int DIV = 41;
  public static final int FLOAT_CONST = 54;
  public static final int PLUS = 38;
  public static final int ASSIGN = 14;
  public static final int MAIN = 5;
  public static final int IF = 25;
  public static final int ID = 53;
  public static final int LE = 47;
  public static final int EOF = 0;
  public static final int RETURN = 34;
  public static final int INT_CONST = 52;
  public static final int TRUE = 35;
  public static final int error = 1;
  public static final int GLOBAL = 10;
  public static final int SRPAR = 31;
  public static final int EQ = 48;
  public static final int TIMES = 40;
  public static final int ARROW = 21;
  public static final int COLON = 9;
  public static final int SHARP = 50;
  public static final int ELSE = 27;
  public static final int READ = 32;
  public static final int WHILE = 23;
  public static final int BLPAR = 19;
  public static final int FLOAT = 17;
  public static final int THEN = 26;
  public static final int NE = 51;
  public static final int END = 8;
  public static final int LPAR = 6;
  public static final int STRING = 18;
  public static final int BOOL_CONST = 3;
  public static final int LOCAL = 29;
  public static final int FALSE = 36;
  public static final int STRING_CONST = 37;
  public static final int GT = 44;
  public static final int DO = 24;
  public static final String[] terminalNames = new String[] {
  "EOF",
  "error",
  "UMINUS",
  "BOOL_CONST",
  "FUNCTION",
  "MAIN",
  "LPAR",
  "RPAR",
  "END",
  "COLON",
  "GLOBAL",
  "SEMI",
  "COMMA",
  "NIL",
  "ASSIGN",
  "INT",
  "BOOL",
  "FLOAT",
  "STRING",
  "BLPAR",
  "BRPAR",
  "ARROW",
  "NOP",
  "WHILE",
  "DO",
  "IF",
  "THEN",
  "ELSE",
  "FOR",
  "LOCAL",
  "SLPAR",
  "SRPAR",
  "READ",
  "WRITE",
  "RETURN",
  "TRUE",
  "FALSE",
  "STRING_CONST",
  "PLUS",
  "MINUS",
  "TIMES",
  "DIV",
  "AND",
  "OR",
  "GT",
  "GE",
  "LT",
  "LE",
  "EQ",
  "NOT",
  "SHARP",
  "NE",
  "INT_CONST",
  "ID",
  "FLOAT_CONST"
  };
}

